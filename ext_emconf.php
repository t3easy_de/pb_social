<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "pb_social".
 *
 * Auto generated 04-05-2016 11:09
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
  'title' => 'Social Media Streams',
  'description' => '',
  'category' => 'plugin',
  'author' => 'Mikolaj Jedrzejewski - plus B, Ramon Mohi - plus B',
  'author_email' => 'info@plusb.de',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '1.1.0',
  'constraints' =>
  array(
    'depends' =>
    array(
      'typo3' => '6.2.0-7.99.99',
      'php' => '5.4.0-7.0.99',
      'extbase' => '',
      'fluid' => '',
    ),
    'conflicts' =>
    array(
    ),
    'suggests' =>
    array(
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => null,
);
